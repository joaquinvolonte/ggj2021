﻿using System;
using Assets.Scripts.Cinematics;
using UnityEngine;

namespace Movement
{
	public class PlayerMovement : MonoBehaviour
	{
		public event Action OnReached;
	
		[SerializeField] float Speed = 2f;
		[SerializeField] float MinDistance = 2f;

		[SerializeField] SmoothRotation SmoothRotation;
		[SerializeField] RockingMovement RockingMovement;
	
		bool isActive;
		
		Vector3 destination;
        private bool endGame;

        void Update()
		{
			if (!isActive)
				return;
			
			if (DestinationReached() && !endGame)
				OnReached();
			else
				transform.Translate(Vector3.forward * (Speed * Time.deltaTime));
		}
		
		public void SetDestination(Vector3 destination)
		{
			this.destination = destination;
			SmoothRotation.SetTarget(destination);
		}

		public void Go()
		{
			Enable();
			SmoothRotation.Enable();
			RockingMovement.Enable();
			Show();
		}
		
		public void Stop()
		{
			isActive = false;
			SmoothRotation.Stop();
			RockingMovement.Stop();
		}

		void Show() => gameObject.SetActive(true);
		void Enable() => isActive = true;
		
		bool DestinationReached() => Vector2.Distance(transform.position, destination) < MinDistance;


        public void SetFinalDestination(Vector3 target)
        {
            SetDestination(target);
            endGame = true;
        }
    }
}

