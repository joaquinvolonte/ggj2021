﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Movement
{
	public class AnimalMovement : MonoBehaviour
	{
		[SerializeField] float MinSpeed;
		[SerializeField] float MaxSpeed;
		
		readonly float LeftBorder = -42;
		readonly float RightBorder = 42;
		readonly float TopBorder = 42;
		readonly float BottomBorder = -42;

		bool diving;
		
		void Update()
		{
			transform.Translate(Vector3.forward * (Random.Range(MinSpeed, MaxSpeed) * Time.deltaTime));

			if (OutOfBounds && !diving) 
				StartCoroutine(DiveAndDestroy());
		}

		IEnumerator DiveAndDestroy()
		{
			Dive();
			yield return new WaitForSeconds(1.2f);
			Destroy(gameObject);
		}

		void Dive()
		{
			diving = true;
			transform.eulerAngles = new Vector3(20, transform.eulerAngles.y, transform.eulerAngles.z);
		}

		bool OutOfBounds =>
			transform.position.x > RightBorder ||
			transform.position.x < LeftBorder ||
			transform.position.z > TopBorder ||
			transform.position.z < BottomBorder;
	}
}