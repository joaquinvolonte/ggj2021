﻿using System;
using UnityEngine;

namespace Movement
{
	public class RockingMovement : MonoBehaviour
	{
		[SerializeField] float bobbingMultiplier = .5f;
		[SerializeField] float rollMultiplier = .4f;
		[SerializeField] Transform Model;
		
		float originalY;
		Quaternion startRotation;
		bool isActive;
		
		public void UpdateStartRotation(Quaternion quaternion) => startRotation = quaternion;

		void Start()
		{
			originalY = Model.position.y;
			startRotation = Model.rotation;
		}

		void Update()
		{
			if (!isActive)
				return;
			
			UpAndDown();
			SideToSide();
			BackAndForth();
		}

		void UpAndDown() => Model.position = new Vector3(Model.position.x,
		                                                 originalY + ((float) Math.Sin(Time.time) * bobbingMultiplier),
		                                                 Model.position.z);

		void SideToSide() => Model.rotation = startRotation * Quaternion.AngleAxis(Mathf.Sin(Time.time * rollMultiplier) * 10f, Vector3.forward);

		void BackAndForth() => Model.rotation = startRotation * Quaternion.AngleAxis(Mathf.Sin(Time.time * rollMultiplier) * 2f, Vector3.left);

		public void Enable() => isActive = true;
		public void Stop() => isActive = false;

	}
}