﻿using UnityEngine;

namespace Movement
{
	public class SmoothRotation: MonoBehaviour
	{
		[SerializeField] float RotationSpeed;
		[SerializeField] RockingMovement RockingMovement;
		
		Vector3 destination;
		Quaternion lookRotation;
		Vector3 direction;

		bool isActive;
		bool shouldRotate;
	
		public void SetTarget(Vector3 destination)
		{
			this.destination = destination;
			shouldRotate = true;
		}

		void Update()
		{
			if (!isActive)
				return;
			
			shouldRotate = transform.rotation != lookRotation; 
			
			direction = (destination - transform.position).normalized;
			lookRotation = Quaternion.LookRotation(direction);

			if (shouldRotate)
			{
				transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * RotationSpeed);
				RockingMovement.UpdateStartRotation(transform.rotation);
			}
		}

		public void Enable() => isActive = true;
		public void Stop() => isActive = false;
	}
}