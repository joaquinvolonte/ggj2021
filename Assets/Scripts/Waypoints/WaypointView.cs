﻿using UnityEngine;

namespace Waypoints
{
	public class WaypointView : MonoBehaviour
	{

		public void SetPosition(Vector3 position) => transform.position = position;
		public void TriggerEnter()
		{
		
		}

		public void Show() => gameObject.SetActive(true);
		public void Hide() => gameObject.SetActive(false);
	}
}