﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Utilities;
using Movement;
using UnityEngine;

namespace Waypoints
{
	public class WaypointManager : MonoBehaviour
	{
		[SerializeField] GameObject OrbitCamera;
		[SerializeField] PlayerMovement PlayerMovement;
		[SerializeField] Transform[] Destinations;
		[SerializeField] GameObject StartMenu;
		[SerializeField] GameObject ConstellationGame;
		[SerializeField] DiscoveryRound DiscoveryRound;
		
		[Header("Views")]
		[SerializeField] WaypointView WaypointView;
		[SerializeField] ButtonsView ButtonsView;
		[SerializeField] HintsView HintsView;

		[Header("Configuration")]
		[SerializeField] int GameLength = 10;
		
		Queue<Vector3> destinations;
		bool IsGameStarted;

		readonly Vector3 ConstellationOffset = new Vector3(0, 5, 0);
		void Awake()
		{
			DetermineDestinations();
			SetNextDestination();
			
			PlayerMovement.OnReached += () =>
			{
				HintsView.ShowHint(Hint.Spyglass);
				PlayerMovement.Stop();
				ButtonsView.EnableSpyglass();
			};

			ButtonsView.OnSpyGlass += () =>
			{
				HintsView.ShowHint(Hint.Zoom);
				ButtonsView.DisableSpyglass();
				OrbitCamera.gameObject.SetActive(false);
				ConstellationGame.gameObject.SetActive(true);
				WaypointView.Hide();
				ConstellationGame.transform.position = PlayerMovement.transform.position + ConstellationOffset;
                DiscoveryRound.StartNewRound();
			};

			DiscoveryRound.OnRoundComplete += () =>
			{
				ButtonsView.EnableRudder();
				HintsView.ShowHint(Hint.Wheel);
			};

			ButtonsView.OnRudder += () =>
			{
				ButtonsView.DisableRudder();
				PlayerMovement.Go();
				OrbitCamera.gameObject.SetActive(true);
				ConstellationGame.gameObject.SetActive(false);
				SetNextDestination();
			};
		}

		void SetNextDestination()
		{
			if (destinations.Any())
			{
				var destination = destinations.Dequeue();
				PlayerMovement.SetDestination(destination);
				WaypointView.SetPosition(destination);
				WaypointView.Show();
				WaypointView.TriggerEnter();
			}
		}

		void Update()
		{
			if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)) && !IsGameStarted)
				StartGame();
		}

		void StartGame()
		{
			IsGameStarted = true;
			HideStartMenu();
			PlayerMovement.Go();
			HintsView.ShowHint(Hint.Camera);
		}

		void DetermineDestinations() => destinations = new Queue<Vector3>(
			                                Destinations
				                                .TakeRandom(GameLength)
				                                .Select(objective => objective.position)
		                                );
		
		void HideStartMenu() => StartMenu.gameObject.SetActive(false);

	}
}