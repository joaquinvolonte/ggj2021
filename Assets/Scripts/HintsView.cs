﻿using TMPro;
using UnityEngine;

public class HintsView : MonoBehaviour
{
	[SerializeField] TextMeshProUGUI Text;
	[SerializeField] string[] Hints;
	
	Hint CurrentHint;
	bool IsComplete;
	
	public void ShowHint(Hint hint)
	{
		if (IsComplete)
			return;

		Show();
		CurrentHint = hint;
		Text.text = Hints[(int)hint];
	}

	void Update()
	{
		if (AdvanceFirstHint || AdvanceSecondHint || AdvanceThirdHint || AdvanceFourthHint || AdvanceFifthHint)
	    {
		    Hide();
		    
		    if(CurrentHint == Hint.Zoom)
			    ShowHint(Hint.Constellations);
	    }

	    if (AdvanceFifthHint) 
		    IsComplete = true;
	}

    bool AdvanceFirstHint => CurrentHint == Hint.Camera && (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0);
    bool AdvanceSecondHint => CurrentHint == Hint.Spyglass && LeftClick;
    bool AdvanceThirdHint => CurrentHint == Hint.Zoom && Spacebar;
    bool AdvanceFourthHint => CurrentHint == Hint.Constellations && LeftClick;
    bool AdvanceFifthHint => CurrentHint == Hint.Wheel && LeftClick;

    static bool Spacebar => Input.GetKeyDown(KeyCode.Space);
    static bool LeftClick => Input.GetMouseButtonDown(0);

    void Show() => gameObject.SetActive(true);
    void Hide() => gameObject.SetActive(false);
}

public enum Hint
{
	Camera,
	Spyglass,
	Zoom,
	Constellations,
	Wheel
}
