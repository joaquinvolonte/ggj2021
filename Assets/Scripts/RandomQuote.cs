﻿using TMPro;
using UnityEngine;

public class RandomQuote : MonoBehaviour
{
	[SerializeField] TextMeshProUGUI Quote;
	[SerializeField] string[] Quotes;

	static readonly System.Random random = new System.Random();
	
	void Awake() => Quote.text = $"{Quotes[random.Next(0, Quotes.Length)]}";
}