﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public static class ExtensionMethods
    {
	    static readonly System.Random random = new System.Random();

        public static T PickOne<T>(this IEnumerable<T> source) => source.ToArray()[Random.Range(0, source.Count() - 1)];

        public static IEnumerable<T> TakeRandom<T>(this IEnumerable<T> source, int amount) => 
	        source.OrderBy(_ => random.Next()).Take(amount);
    }
}
