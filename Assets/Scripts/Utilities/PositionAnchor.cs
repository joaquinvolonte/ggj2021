﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class PositionAnchor : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float stickyness;

        void Update()
        {
            if(target)
                transform.position = Vector3.Lerp(transform.position, target.position, stickyness);
        }

        public void SetTarget(Transform target) => 
            this.target = target;

        public void UnLink() => 
            target = null;
    }
}
