﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsView : MonoBehaviour
{
	public event Action OnSpyGlass;
	public event Action OnRudder;
		
	[SerializeField] Button Spyglass;
	[SerializeField] Button Rudder;

	void Awake()
	{
		Spyglass.onClick.AddListener(() => OnSpyGlass());
		Rudder.onClick.AddListener(() => OnRudder());
	}

	public void EnableSpyglass() => Spyglass.interactable = true;
	public void DisableSpyglass() => Spyglass.interactable = false;
	
	public void EnableRudder() => Rudder.interactable = true;
	public void DisableRudder() => Rudder.interactable = false;
}