﻿using System.Collections.Generic;
using Assets.Scripts.Utilities;

namespace Assets.Scripts.Sky
{
    public class CelestialNames
    {
        private static List<string> names = new List<string>()
        {
            "The Bull", "The Mother", "The Wise", "The Jammer", "The Sculptor", "The Hunter", "The Mistress", "The King", "The Queen", "The Peasant",
            "The Joker", "The Gambit", "The Castle", "Winter", "Summer", "Spring", "Autum", "The Trader", "The Pirate", "The Admiral", "The General",
            "Medusa", "Yisera", "Draco", "Ragnaros", "Malfurion", "Cerberus", "Hades", "Charon", "Jupiter", "Odin", "Thor", "Ozzy", "Lemmy", "Van Halen",
            "Hydra", "Lupus", "Circinus", "Cassiopeia", "Pegasus", "Orion", "Perseus", "Phoenix", "Hercules", "Scutum", "Serpens", "Sextans",
            "Aquarius", "Scorpius", "Cancer", "Aries", "Capricornus", "Gemini", "Leo", "Libra", "Pisces", "Sagittarius", "Taurus", "Virgo"
        };

        public static string GetAName()
        {
            var name = names.PickOne();
            names.Remove(name);

            return name;
        }
    }
}
