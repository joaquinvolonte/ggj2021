﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Sky
{
    public class SkyGenerator : MonoBehaviour
    {
	    [SerializeField] private Constellation constellationPrefab;
        [SerializeField] private DecoyStarDome starDome;
        [SerializeField] private Transform decoyStarContainer;
        [SerializeField] private float skySize;
        [SerializeField] private int constellationCount;
        [SerializeField] private float constellationSeparation;
        [SerializeField] private int constellationPlacementTries;
        [SerializeField] private SkyAudioPlayer skyAudioPlayer;
        [SerializeField] private DiscoveryRound discoveryRound;

        List<Constellation> constellations = new List<Constellation>();

        void Awake()
        {
            starDome.Generate(decoyStarContainer, skySize);
            GenerateConstellations();
            discoveryRound.Initialize(constellations);
        }

        void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.F))
            {
                foreach (var c in constellations)
                    c.ForceSolve();
            }
#endif
        }

        private void GenerateConstellations()
        {
            foreach (var _ in Enumerable.Range(0, constellationCount))
            {
                var constellation = Instantiate(constellationPrefab, transform);
                constellation.Initialize(skyAudioPlayer);
                
                PlaceConstellation(constellation);

                constellations.Add(constellation);
                constellation.transform.LookAt(transform.position);
            }
        }

        private Vector3 RandomDomePoint()
        {
            var radius = Random.insideUnitSphere.normalized;
            radius.y = radius.y < 0 ? -radius.y : radius.y;
            var newPos = radius * skySize;
            return newPos;
        }

        private void PlaceConstellation(Constellation constellation)
        {
            var targetPosition = transform.position + RandomDomePoint();
            var numberOfTries = 0;

            while (CantPlaceHere(targetPosition) && numberOfTries < constellationPlacementTries)
            {
                targetPosition = transform.position + RandomDomePoint();
                numberOfTries++;
            }

            constellation.transform.position = targetPosition;
        }

        private bool CantPlaceHere(Vector3 targetPosition) => 
            constellations.Select(c => c.transform.position).Any(cp => Vector3.Distance(cp, targetPosition) < constellationSeparation);
        
    }
}
