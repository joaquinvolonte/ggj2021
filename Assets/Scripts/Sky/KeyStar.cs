﻿using System;
using UnityEngine;

namespace Assets.Scripts.Sky
{
    public class KeyStar : Star
    {
        [SerializeField] private KeyDetection detector;
        [SerializeField] private ParticleSystem rippleVfx;

        public void Initialize(Action<KeyStar> onStarClicked, Action<float> playProbe)
        {
            base.Initialize();
            detector.Initialize(() => { onStarClicked(this);}, playProbe);
        }

        public void Inhibit()
        {
            detector.enabled = false;
            rippleVfx.Stop();
        }

        public void Activate()
        {
            detector.enabled = true;
            rippleVfx.Play();
        }
    }
}