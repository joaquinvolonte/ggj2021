﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Sky
{
    public class Constellation : MonoBehaviour
    {
	    [SerializeField] KeyStar starPrefab;
        [SerializeField] private LineRenderer linePrefab;
        [SerializeField] int minCount;
        [SerializeField] int maxCount;
        [SerializeField] private float size;
        [SerializeField] private ParticleSystem winVfx;
        [SerializeField] private ConstellationDisplay nameDisplay;
        
        public string constellationName;
        private ConstellationDrawer drawer;
        private KeyStar lastClickedStar;
        private List<KeyStar> discoveredStars = new List<KeyStar>();
        private List<KeyStar> stars = new List<KeyStar>();
        private int starCount;
        private SkyAudioPlayer audioPlayer;
        private Action onDiscovery = () => { };
        public bool WasDiscovered;
        public Vector3 Position => transform.position;

        public bool isActive;

        public void Initialize(SkyAudioPlayer audioPlayer)
        {
            this.audioPlayer = audioPlayer;
            constellationName = CelestialNames.GetAName();
            drawer = new ConstellationDrawer(linePrefab, transform);
            starCount = Random.Range(minCount, maxCount);

            foreach (var _ in Enumerable.Range(0, starCount))
            {
                var star = Instantiate(starPrefab, transform);
                star.transform.position = Random.insideUnitCircle * size;
                star.transform.position += transform.position;
                star.Initialize(OnStarClicked, audioPlayer.PlayProbe);
                stars.Add(star);
            }

            InhibitInteraction();
        }

        public void OnStarClicked(KeyStar newStar)
        {
            if (!discoveredStars.Contains(newStar))
            {
                if (lastClickedStar)
                {
                    drawer.DrawLine(lastClickedStar, newStar);

                    if(IsTributeConstellation)
                        audioPlayer.PlayTributeTouch(discoveredStars.Count - 1);
                    else
                        audioPlayer.PlayTouch(discoveredStars.Count -1);
                }

                lastClickedStar = newStar;
                discoveredStars.Add(newStar);

                if (discoveredStars.Count >= starCount)
                    ConstellationDiscovered();
            }
        }

        private bool IsTributeConstellation => constellationName.Equals("Van Halen");

        private void ConstellationDiscovered()
        {
            WasDiscovered = true;
            nameDisplay.DisplayName(constellationName);
            winVfx.Play();

            if(IsTributeConstellation)
                audioPlayer.PlayTributeDiscover();
            else
                audioPlayer.PlayDiscover();

            onDiscovery();
            InhibitInteraction();
        }

        public void ForceSolve()
        {
            foreach (var s in stars.Take(stars.Count - 1))
                OnStarClicked(s);

            OnStarClicked(stars.Last());
        }

        public void ActivateForRound(Action onDiscovery)
        {
            Activate();
            this.onDiscovery = onDiscovery;
        }

        void InhibitInteraction()
        {
            isActive = false;
            foreach (var s in stars)
                s.Inhibit();
        }

        void Activate()
        {
            isActive = true;
            foreach (var s in stars)
                s.Activate();
        }
    }
}