﻿using Assets.Scripts.Utilities;
using UnityEngine;

namespace Assets.Scripts.Sky
{
    public class Star : MonoBehaviour
    {
        [SerializeField] protected Material[] materials;
        [SerializeField] protected ParticleSystem particle;
        [SerializeField] protected Gradient spectrum;
        [SerializeField] protected float minSize;
        [SerializeField] protected float maxSize;

        public void Initialize()
        {
            var mainParticle = particle.main;
            mainParticle.startColor = spectrum.Evaluate(Random.value);
            mainParticle.startSize = Random.Range(minSize, maxSize);
            var renderer = particle.GetComponent<Renderer>();
            renderer.materials = new[] {materials.PickOne()};

            particle.Play();
        }
    }
}
