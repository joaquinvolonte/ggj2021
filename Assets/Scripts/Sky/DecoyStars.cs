﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Sky
{
    [Serializable]
    public class DecoyStars
    {
        [SerializeField] Star starPrefab;
        [SerializeField] int minCount;
        [SerializeField] int maxCount;

        public void Generate(Transform container, float size)
        {
            var starCount = Random.Range(minCount, maxCount);

            foreach (var _ in Enumerable.Range(0, starCount))
            {
                var star = GameObject.Instantiate(starPrefab, container);
                star.transform.position = Random.insideUnitCircle * size;
                star.transform.position += container.position;
                star.Initialize();
            }
        }
    }


    namespace Assets.Scripts.Sky
    {
    }
}