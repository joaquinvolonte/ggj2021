﻿using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Sky
{
    [Serializable]
    public class ConstellationDisplay
    {
        [SerializeField] private TextMeshProUGUI nameDisplay;

        public void DisplayName(string name)
        {
            nameDisplay.text = name;
            nameDisplay.gameObject.SetActive(true);
        }
    }
}
