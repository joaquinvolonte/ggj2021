﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Sky
{
    public class KeyDetection : MonoBehaviour
    {
        private Action onClicked;
        [SerializeField] private ParticleSystem ripple;
        private Action<float> onProbe;

        public void Initialize(Action onClicked, Action<float> onProbe)
        {
            this.onProbe = onProbe;
            this.onClicked = onClicked;
        }

        void OnMouseEnter()
        {
            if (enabled)
            {
                ripple.Emit(1);
                onProbe(Random.Range(0.8f, 1.2f));
            }
        }

        void OnMouseDown()
        {
            if (enabled)
            {
                onClicked();
            }
        }
    }
}
