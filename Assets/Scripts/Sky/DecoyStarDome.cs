﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Sky
{
    [Serializable]
    public class DecoyStarDome
    {
        [SerializeField] Star starPrefab;
        [SerializeField] int starCount;

        public void Generate(Transform container, float size)
        {

            foreach (var _ in Enumerable.Range(0, starCount))
            {
                var star = GameObject.Instantiate(starPrefab, container);
                var radius = Random.insideUnitSphere.normalized;
                
                radius.y = radius.y < 0 ? -radius.y : radius.y;
                star.transform.position = radius * size;

                star.transform.position += container.position;
                star.Initialize();
            }
        }
    }
}