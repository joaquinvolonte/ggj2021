﻿using Assets.Scripts.Utilities;
using UnityEngine;

namespace Assets.Scripts.Sky
{
    public class SkyAudioPlayer : MonoBehaviour
    {
        [SerializeField] private AudioClip[] touchSamples;
        [SerializeField] private AudioClip[] tributeTouchSamples;
        [SerializeField] private AudioClip[] probeSamples;

        [SerializeField] private AudioSource probeSfx;
        [SerializeField] private AudioSource touchSfx;
        [SerializeField] private AudioSource tributeTouchSfx;
        [SerializeField] private AudioSource discoverSfx;
        [SerializeField] private AudioSource tributeDiscoverSfx;

        public void PlayProbe(float pitch)
        {
            probeSfx.Stop();
            probeSfx.clip = probeSamples.PickOne();
            probeSfx.Play();
        }
        public void PlayTouch(int starIndex)
        {
            touchSfx.Stop();
            touchSfx.clip = touchSamples[starIndex];
            touchSfx.Play();
        }
        public void PlayDiscover()
        {
            discoverSfx.Stop();
            discoverSfx.Play();
        }

        public void PlayTributeDiscover()
        {
            tributeDiscoverSfx.Stop();
            tributeDiscoverSfx.Play();
        }

        public void PlayTributeTouch(int starIndex)
        {
            tributeTouchSfx.Stop();
            tributeTouchSfx.clip = tributeTouchSamples[starIndex];
            tributeTouchSfx.Play();
        }
    }
}
