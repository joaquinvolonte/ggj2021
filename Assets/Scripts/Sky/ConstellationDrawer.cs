﻿using UnityEngine;

namespace Assets.Scripts.Sky
{
    public class ConstellationDrawer
    {
        private LineRenderer linePrefab;
        private Transform container;

        public ConstellationDrawer(LineRenderer line, Transform container)
        {
            this.container = container;
            this.linePrefab = line;
        }

        public void DrawLine(KeyStar star1, KeyStar star2)
        {
            var line = GameObject.Instantiate(linePrefab, container);
            var pos1 = container.InverseTransformPoint(star1.transform.position);
            var pos2 = container.InverseTransformPoint(star2.transform.position);

            line.SetPositions(new[] {pos1, pos2});
        }
    }
}
