﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Trash
{
    public class TrashCameraPan : MonoBehaviour
    {
        [SerializeField] Camera camera;
        [SerializeField] private float yawSpeed;
        [SerializeField] private float pitchSpeed;
        [SerializeField] private float fovZoomedIn;
        [SerializeField] private float zoomInSpeed;
        [SerializeField] private Image spyglass;

        private float initialFov;
        private bool zoomedIn;
        private bool isZooming;

        void Start() => initialFov = camera.fieldOfView;

        void Update()
        {
            var deltaYaw = Input.GetAxis("Horizontal");
            var deltaPitch = Input.GetAxis("Vertical");
            
            transform.Rotate(Vector3.up * deltaYaw * yawSpeed);
            transform.Rotate(Vector3.right * -deltaPitch * pitchSpeed);

            NormalizeRoll();

            if (Input.GetButtonDown("Zoom"))
                TriggerZoom();
        }

        void NormalizeRoll()
        {
            var euler = transform.eulerAngles;
            euler.z = 0;
            transform.eulerAngles = euler;
        }

        void TriggerZoom()
        {
            if (!isZooming)
            {
                if (zoomedIn)
                    DisengageZoom();
                else
                    EngageZoom();

                isZooming = true;
                zoomedIn = !zoomedIn;
            }
        }

        void EngageZoom()
        {
            StartCoroutine(ToggleZoom(fovZoomedIn));
            StartCoroutine(ToggleSpyglass(1));
        }

        void DisengageZoom()
        {
            StartCoroutine(ToggleZoom(initialFov));
            StartCoroutine(ToggleSpyglass(0));
        }

        IEnumerator ToggleZoom(float targetFov)
        {
            while (Mathf.Abs(camera.fieldOfView - targetFov) > .2f)
            {
                camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, targetFov, zoomInSpeed);

                yield return null;
            }

            isZooming = false;
        }

        IEnumerator ToggleSpyglass(float targetAlpha)
        {
            while (Mathf.Abs(spyglass.color.a - targetAlpha) > .05f)
            {
                var color = spyglass.color;
                color.a = Mathf.Lerp(color.a, targetAlpha, zoomInSpeed);
                spyglass.color = color;

                yield return null;
            }

            var color2 = spyglass.color;
            color2.a = targetAlpha;
            spyglass.color = color2;
        }

        public void LookTo(Vector3 target)
        {
            transform.LookAt(target);
            NormalizeRoll();
        }
    }
}
