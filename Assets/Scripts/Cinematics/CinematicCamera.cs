﻿using UnityEngine;

namespace Assets.Scripts.Cinematics
{
    public class CinematicCamera : MonoBehaviour
    {
        [SerializeField] private Transform target;

        private bool stargazeMode;
        private float stargazeFactor = 0.006f;

        void Update()
        {
            if (stargazeMode)
                Stargaze();
            else
                FollowTarget();
        }

        private void Stargaze()
        {
            transform.Rotate(Vector3.up * stargazeFactor);
            NormalizeRoll();
        }

        void FollowTarget()
        {
            transform.LookAt(target);
            NormalizeRoll();
        }

        public void SwitchMode() => stargazeMode = true;

        void NormalizeRoll()
        {
            var euler = transform.eulerAngles;
            euler.z = 0;
            transform.eulerAngles = euler;
        }
    }
}