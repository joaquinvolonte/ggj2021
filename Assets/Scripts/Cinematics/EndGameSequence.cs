﻿using System.Collections;
using Assets.Scripts.Utilities;
using Movement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Cinematics
{
    public class EndGameSequence : MonoBehaviour
    {
        [SerializeField] Image fadeIn;
        [SerializeField] float fadeInSpeed;

        [SerializeField] private GameObject skyCamera;
        [SerializeField] private GameObject boatCamera;
        [SerializeField] private CinematicCamera sequenceCamera;
        [SerializeField] private GameObject credits;
        [SerializeField] private GameObject mainCanvas;
        [SerializeField] private GameObject skyCanvas;

        [SerializeField] private PositionAnchor cameraTarget;
        [SerializeField] private PositionAnchor fogRing;
        [SerializeField] private PlayerMovement boat;

        [Header("Audio")] 
        [SerializeField] AudioSource MusicSource;
        [SerializeField] AudioClip WinClip;
        
        bool hasWon;
        
        public void PlaySequence()
        {
            StartCoroutine(FadeIn());
            StartCoroutine(WaitForBoat());
            StartCoroutine(SwapMusic());
            fogRing.UnLink();
            boat.SetFinalDestination(new Vector3(200, 0, 200));
            boat.Go();
            mainCanvas.SetActive(false);
            skyCanvas.SetActive(false);
            skyCamera.SetActive(false);
            boatCamera.SetActive(false);
            sequenceCamera.gameObject.SetActive(true);
            sequenceCamera.transform.position = boat.transform.position + new Vector3(3, 5, 3);

            cameraTarget.SetTarget(boat.transform);
            hasWon = true;
        }

        IEnumerator SwapMusic()
        {
	        while (MusicSource.volume > .1)
	        {
		        MusicSource.volume -= .1f;
				yield return new WaitForSeconds(.1f);
	        }

	        MusicSource.clip = WinClip;
	        MusicSource.Play();

	        while (MusicSource.volume < 1)
	        {
		        MusicSource.volume += 1;
				yield return new WaitForSeconds(.1f);
	        }
        }

        private IEnumerator WaitForBoat()
        {
            yield return new WaitForSeconds(10);
            credits.SetActive(true);
            StartCoroutine(PanCameraUp());
        }

        private IEnumerator PanCameraUp()
        {
            cameraTarget.UnLink();
            while (cameraTarget.transform.position.y < 30f)
            {               
                cameraTarget.transform.position += Vector3.up * 0.02f;
                yield return null;
            }

            sequenceCamera.SwitchMode();
        }

        void Update()
        {
	        if (hasWon && Input.GetKeyDown(KeyCode.Escape))
		        Restart();
        }

        static void Restart() => SceneManager.LoadScene("Gameplay");

        IEnumerator FadeIn()
        {
            while (fadeIn.color.a > 0)
            {
                var col = fadeIn.color;
                col.a -= fadeInSpeed;
                fadeIn.color = col;

                yield return null;
            }
        }
    }
}
