﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Cinematics;
using Assets.Scripts.Sky;
using Assets.Scripts.Trash;
using Assets.Scripts.Utilities;
using UnityEngine;

public class DiscoveryRound : MonoBehaviour
{
    [SerializeField] private int roundLength;
    [SerializeField] private TrashCameraPan camera;
    [SerializeField] private EndGameSequence endGame;

    private List<Constellation> constellations;
    public IEnumerable<Constellation> roundConstellations;
    
    private IEnumerable<Constellation> UndiscoveredConstellations => 
        constellations.Where(c => !c.WasDiscovered);

    public event Action OnRoundComplete;

    public void Initialize(List<Constellation> constellations) => 
        this.constellations = constellations;

    public void StartNewRound()
    {
        var startingConstellation = UndiscoveredConstellations.PickOne();

        camera.LookTo(startingConstellation.Position);

        roundConstellations =
            UndiscoveredConstellations
            .Select(c => new {cons = c, distance = Vector3.Distance(c.Position, startingConstellation.Position)})
            .OrderBy(cd => cd.distance)
            .Take(roundLength)
            .Select(cd => cd.cons)
            .ToArray();
        
      foreach (var c in roundConstellations)
            c.ActivateForRound(OnConstellationDiscovered);

    }

    void OnConstellationDiscovered()
    {
        if (!UndiscoveredConstellations.Any()) 
            endGame.PlaySequence();

        else if (roundConstellations.All(c => c.WasDiscovered))
            OnRoundComplete();
    }

    void Update()
	{
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.J))
			OnRoundComplete();
        if (Input.GetKeyDown(KeyCode.B))
            endGame.PlaySequence();
#endif
    }
}