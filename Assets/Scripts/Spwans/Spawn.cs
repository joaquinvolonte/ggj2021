﻿using System;
using UnityEngine;

namespace Spwans
{
	[Serializable]
	public class Spawn
	{
		public Vector3 Direction;
		public Transform Transform;
	}
}	