﻿using System.Collections;
using UnityEngine;

namespace Spwans
{
	public class AnimalsManager : MonoBehaviour
	{
		[SerializeField] GameObject[] Prefabs;
		[SerializeField] Spawn[] SpawnPoints;

		[SerializeField] float MinTime = 15;
		[SerializeField] float MaxTime = 30;

		[SerializeField] bool canSpawn = true;
		
		static readonly System.Random random = new System.Random();
	
		void Awake()
		{
			StartCoroutine(InitialSpawn());
			StartCoroutine(Spawner());
		}

		IEnumerator InitialSpawn()
		{
			Spawn();
			yield return new WaitForSeconds(1);
			Spawn();
			yield return new WaitForSeconds(1);
			Spawn();
		}

		IEnumerator Spawner()
		{
			while (canSpawn)
			{
				yield return new WaitForSeconds(Random.Range(MinTime,MaxTime));
				Spawn();
			}
		}

		void Spawn()
		{
			var prefab = Prefabs[random.Next(Prefabs.Length)];
			var spawnPoint = SpawnPoints[random.Next(SpawnPoints.Length)];

			var animal = Instantiate(prefab, spawnPoint.Transform.position, Quaternion.identity);
			animal.transform.LookAt(spawnPoint.Direction);
		}
	}
}