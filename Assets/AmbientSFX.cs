﻿using System.Collections;
using Assets.Scripts.Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

public class AmbientSFX : MonoBehaviour
{	
	[SerializeField] AudioSource AudioSource;
	[SerializeField] AudioClip[] AudioClips;
	
	[SerializeField] float MinTime = 12f;
	[SerializeField] float MaxTime = 20f;

	void Awake() => StartCoroutine(PlayWaveSounds(Random.Range(MinTime, MaxTime)));

	IEnumerator PlayWaveSounds(float seconds)
	{
		while (true)
		{
			var clip = AudioClips.PickOne();
			AudioSource.clip = clip;
			AudioSource.Play();
			yield return new WaitForSeconds(seconds);
		}
	}
}
